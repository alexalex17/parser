<?php
/**
 * IAAI Handler
 *
 * @category  Parser
 * @package   Parser\Handlers
 */

namespace Parser\Handlers;

use Parser\ParserAbstract;
use Parser\ParserInterface;
use Exception;

/**
 * Class IAAIHandler
 *
 * @package Parser
 */
class IAAIHandler extends ParserAbstract implements ParserInterface
{
    /**#@+
     * Keys
     */
    const PRICE_KEY = 'price';
    const ENGINE_CAPACITY_KEY = 'engine_capacity';
    const YEAR_KEY = 'year';
    const REGION_KEY = 'region';
    /**#@-*/

    /**
     * Get content
     *
     * @param string $method
     * @param array|null $headers
     * @param string $body
     * @return array|\DOMDocument
     *
     * @throws Exception
     */
    public function getContent($method = self::HTTP_METHOD_GET, array $headers = null, $body = '')
    {
        $dom = parent::getContent($method, $headers, $body);
        //$dom->getElementById('chromeData')->getElementsByTagName('h2')->item(0)->nodeValue;


        return [
            self::PRICE_KEY => '',
            self::ENGINE_CAPACITY_KEY => '',
            self::YEAR_KEY => '',
            self::REGION_KEY => ''
        ];
    }
}
