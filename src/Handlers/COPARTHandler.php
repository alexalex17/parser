<?php
/**
 * COPART Handler
 *
 * @category  Parser
 * @package   Parser\Handlers
 */

namespace Parser\Handlers;

use Parser\ParserAbstract;
use Parser\ParserInterface;
use Exception;
use WebBrowser;

/**
 * Class COPARTHandler
 *
 * @package Parser\Handlers
 */
class COPARTHandler extends ParserAbstract implements ParserInterface
{
    /**#@+
     * Keys
     */
    const PRICE_KEY = 'price';
    const ENGINE_CAPACITY_KEY = 'engine_capacity';
    const YEAR_KEY = 'year';
    const REGION_KEY = 'region';
    /**#@-*/

    /**#@+
     * Node API data
     */
    const NODE_API_HOST = '68.183.20.147';
    const NODE_API_PORT = '3002';
    /**#@-*/

    /**
     * Get content
     *
     * @param string $method
     * @param array|null $headers
     * @param string $body
     * @return array|\DOMDocument
     *
     * @throws Exception
     */
    public function getContent($method = self::HTTP_METHOD_POST, array $headers = null, $body = '')
    {
        $dom = parent::getContent($method, $headers, $body);
        return [
            self::PRICE_KEY => $this->_getPrice($dom),
            self::ENGINE_CAPACITY_KEY => $this->_getEngine($dom),
            self::YEAR_KEY => explode(' ', trim($dom->getElementsByTagName('h1')->item(0)->textContent))[0],
            self::REGION_KEY => $this->_getRegion($dom)
        ];
    }

    /**
     * Prepare curl
     *
     * @param string $method
     *
     * @throws Exception
     */
    protected function _prepareCurl($method = self::HTTP_METHOD_GET)
    {
        parent::_prepareCurl($method);

        curl_setopt($this->_curl, CURLOPT_URL, $this->_getNodeApiFullUrl());
        curl_setopt(
            $this->_curl, CURLOPT_POSTFIELDS,
            "url={$this->getCurrentUrl()}"
        );
    }

    /**
     * Get response type
     *
     * @return string
     */
    public function getResponseType()
    {
        return static::RESPONSE_TYPE_JSON;
    }

    /**
     * Get price
     *
     * @param \DOMDocument $dom
     *
     * @return int|string
     */
    protected function _getPrice($dom)
    {
        $price = 'Can\'t parse price';
        $divsPrice = $dom->getElementsByTagName('div');
        for ($i = 0; $i < $divsPrice->length - 1; $i++) {
            $item = $divsPrice->item($i);
            if ($item->getAttribute('class') == 'col-md-5 pull-right no-padding col-sm-12 col-xs-12 bid-right-col') {
                $price = explode(' ', $item->getElementsByTagName('span')->item(2)->textContent)[0];
                $price = trim($price, '$');
                break;
            }
        }

        return $price;
    }

    /**
     * Get engine
     *
     * @param \DOMDocument $dom
     *
     * @return string
     */
    protected function _getEngine($dom)
    {
        $engine = 'No engine';
        $engineDiv = $dom->getElementsByTagName('div');
        for ($i = 0; $i < $engineDiv->length - 1; $i++) {
            $item = $engineDiv->item($i);
            if ($item->getAttribute('class') == 'formbox sale-info-box') {
                $engine = $item->getElementsByTagName('span')->item(2)->textContent;
                break;
            }
        }

        return $engine;
    }

    /**
     * Get region
     *
     * @param \DOMDocument $dom
     *
     * @return string
     */
    protected function _getRegion($dom)
    {
        $region = 'No region';
        $index = 0;
        $engineDiv = $dom->getElementsByTagName('div');
        for ($i = 0; $i < $engineDiv->length - 1; $i++) {
            $item = $engineDiv->item($i);
            if ($item->getAttribute('class') == 'formbox sale-info-box') {
                $index += 1;
                if ($index == 2) {
                    $region = $item->getElementsByTagName('span')->item(0)->textContent;
                    $region = explode('-', $region)[0];
                    break;
                }
            }
        }

        return $region;
    }

    /**
     * Get Node API full url
     *
     * @return string
     */
    protected function _getNodeApiFullUrl()
    {
        return $this->_getNodeApiUrl() . ':' . $this->_getNodeApiPort();
    }

    /**
     * Get node api url
     *
     * @return string
     */
    protected function _getNodeApiUrl()
    {
        return static::NODE_API_HOST;
    }

    /**
     * Get node apu port
     *
     * @return string
     */
    protected function _getNodeApiPort()
    {
        return static::NODE_API_PORT;
    }
}
