<?php
/**
 * Parser Delegator
 *
 * @category  Parser
 * @package   Parser
 */

namespace Parser;

use Exception;

/**
 * Class ParserDelegator
 *
 * @package Parser
 */
class ParserDelegator implements ParserInterface
{
    /**
     * Handler dir name
     */
    const HANDLERS_DIR_NAME = 'Handlers';

    /**
     * Current Url
     *
     * @var string
     */
    protected $_currentUrl = null;

    /**
     * Parser constructor.
     *
     * @param string $url
     */
    public function __construct($url = null)
    {
        if ($url) {
            $this->setCurrentUrl($url);
        }
    }

    /**
     * Get content
     *
     * @param string $method
     * @param array|null $headers
     * @param string $body
     *
     * @return mixed
     */
    public function getContent($method = self::HTTP_METHOD_GET, array $headers = null, $body = '')
    {
        $data = parse_url($this->getCurrentUrl());
        $hostArray = explode('.', $data['host']);
        $host = $hostArray[0];
        if ($hostArray[0] == 'www') {
            $host = $hostArray[1];
        }
        $className = strtoupper($host) . 'Handler';
        if (file_exists(__DIR__ . '/' . self::HANDLERS_DIR_NAME . '/' . $className . '.php')) {
            $className = '\\Parser\\Handlers\\' . $className;
            $handler = new $className($this->getCurrentUrl());
            if ($handler instanceof ParserInterface) {
                return $handler->getContent($method, $headers, $body);
            }
        }
    }

    /**
     * Get current url
     *
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->_currentUrl;
    }

    /**
     * Swt current url
     *
     * @param string $currentUrl
     *
     * @return ParserDelegator
     */
    public function setCurrentUrl($currentUrl)
    {
        $this->_currentUrl = $currentUrl;

        return $this;
    }
}
