<?php
/**
 * Parser abstract
 *
 * @category  Parser
 * @package   Parser
 * @author    Vitaliy Pyatin <mail.pyvil@gmail.com>
 * @copyright Vitaliy Pyatin
 */

namespace Parser;

use DOMDocument;
use Exception;

/**
 * Class ParserAbstract
 *
 * @package Parser
 */
abstract class ParserAbstract implements ParserInterface
{
    /**#@+
     * Response types
     */
    const RESPONSE_TYPE_HTML = 'text/html';
    const RESPONSE_TYPE_JSON = 'json';
    /**#@-*/

    /**
     * Current url
     *
     * @var string
     */
    protected $_currentUrl = '';

    /**
     * Curl
     *
     * @var resource
     */
    protected $_curl = null;

    /**
     * Curl wait timeout
     *
     * @var int
     */
    protected $_timeout = 5;

    /**
     * Headers
     *
     * @var array
     */
    protected $_headers = null;

    /**
     * DOM
     *
     * @var DOMDocument
     */
    protected $_dom = null;

    /**
     * Allowed HTTP methods
     *
     * @var array
     */
    protected $_allowedHttpMethods = [
        self::HTTP_METHOD_GET,
        self::HTTP_METHOD_POST
    ];

    /**
     * ParserAbstract constructor.
     *
     * @param string $currentUrl
     */
    public function __construct($currentUrl = null)
    {
        if ($currentUrl) {
            $this->_currentUrl = $currentUrl;
        }
    }

    /**
     * @param int $timeout
     * @return ParserAbstract
     */
    public function setTimeout(int $timeout)
    {
        $this->_timeout = $timeout;

        return $this;
    }

    /**
     * Set headers
     *
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        $this->_headers = $headers;
    }

    /**
     * Set current url
     *
     * @param string $currentUrl
     *
     * @return ParserAbstract
     */
    public function setCurrentUrl($currentUrl)
    {
        $this->_currentUrl = $currentUrl;

        return $this;
    }

    /**
     * Parse
     *
     * @param string $method
     * @param array $headers
     * @param string $body
     *
     * @return DOMDocument
     * @throws Exception
     */
    public function getContent($method = self::HTTP_METHOD_GET, array $headers = null, $body = '')
    {
        if (!in_array($method, $this->_allowedHttpMethods)) {
            throw new Exception('Not allowed http method');
        }
        if ($headers) {
            $this->setHeaders($headers);
        }

        $this->_prepareCurl($method);
        $response = $this->_getParsedResponse(curl_exec($this->_curl));

        if (!$this->getDom()->loadHTML($response, LIBXML_NOWARNING | LIBXML_NOERROR)) {
            throw new Exception('Cannot parse.');
        }

        return $this->getDom();
    }

    /**
     * Get parsed response
     *
     * @param $response
     *
     * @return string
     */
    protected function _getParsedResponse($response)
    {
        $result = null;
        switch ($this->getResponseType()) {
            case static::RESPONSE_TYPE_HTML:
                $result = $response;
                break;

            case static::RESPONSE_TYPE_JSON:
                $data = (array) json_decode(trim($response, '\''));
                if ($data['status'] == 200) {
                    $result = $data['html'];
                }
                break;
        }

        return $result;
    }

    /**
     * Prepare curl
     *
     * @param string $method
     *
     * @throws Exception
     */
    protected function _prepareCurl($method = self::HTTP_METHOD_GET)
    {
        if (!$this->getCurrentUrl()) {
            throw new Exception('No url to parse');
        }

        $this->_curl = curl_init();
        curl_setopt($this->_curl, CURLOPT_URL, $this->getCurrentUrl());
        curl_setopt($this->_curl, CURLOPT_CONNECTTIMEOUT, $this->_timeout);
        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->_curl, CURLOPT_FOLLOWLOCATION, true);
        if (!is_null($this->getHeaders())) {
            curl_setopt($this->_curl, CURLOPT_HTTPHEADER, $this->getHeaders());
        }
        if ($method == static::HTTP_METHOD_POST) {
            curl_setopt($this->_curl, CURLOPT_POST, true);
        } elseif ($method == static::HTTP_METHOD_POST) {
            curl_setopt($this->_curl, CURLOPT_HTTPGET, true);
        }
        if (!is_null($this->getHeaders())) {
            curl_setopt($this->_curl, CURLOPT_HEADER, true);
        }
        curl_setopt($this->_curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->_curl, CURLOPT_SSL_VERIFYHOST, 0);

    }

    /**
     * Response type
     *
     * @return string
     */
    public function getResponseType()
    {
        return static::RESPONSE_TYPE_HTML;
    }

    /**
     * Get headers
     *
     * @return array|null
     */
    public function getHeaders()
    {
        return $this->_headers;
    }

    /**
     * Get current url
     *
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->_currentUrl;
    }

    /**
     * Get DOM
     *
     * @return DOMDocument
     */
    public function getDom()
    {
        if (is_null($this->_dom)) {
            $this->_dom = new DOMDocument('1.0', 'UTF-8');
        }
        return $this->_dom;
    }

    /**
     * Destruct
     */
    public function __destruct()
    {
        curl_close($this->_curl);
    }
}
