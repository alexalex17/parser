<?php
/**
 * Parser
 *
 * @category  Parser
 * @package   Parser
 */
namespace Parser;

/**
 * Interface ParserInterface
 *
 * @package Parser
 */
interface ParserInterface
{
    /**#@+
     * HTTP methods
     */
    const HTTP_METHOD_GET = 'GET';
    const HTTP_METHOD_POST = 'POST';
    /**#@-*/

    /**
     * Get content
     *
     * @param string $method
     * @param array|null $headers
     * @param string $body
     *
     * @return \DOMDocument
     */
    public function getContent($method = self::HTTP_METHOD_GET, array $headers = null, $body = '');

}